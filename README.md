# Wardrobify

Team:

* Person 1 - Taylor Panning - Shoes
* Person 2 - Stephen Ho - Hats

## Design

## Shoes microservice

For the shoes microservice I am going to start by building a model for the shoes. Then I will create an API to send my data on my shoes. I will then test to see if I can display a list of shoes in the browser and check insomnia to make sure my model is working. Once that basic component is done, I can worry about deleting, and adding new shoes sections. 

## Hats microservice

Hats will pull the api from wardrobify. It will show a list of of hats
which will include the fabric, style name, color, a picture of the hat, and the location of the hat in the wardrobe.
You will also be able to create and delete a hat. The created hat will be added
to the list of hats. You will be able to choose a hat to delete. The Url, Views, and Model page will work together to display a webpage that shows the hat, details, and the layout of the page. This will all be done with react. 
