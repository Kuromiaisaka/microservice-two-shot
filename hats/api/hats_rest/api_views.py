from .models import Hats, LocationVO
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "section_number", "shelf_number"]

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = ["name", "fabric", "style", "color", "picture_url", "location"]
    encoders = {"location": LocationVOEncoder(),}

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatsDetailEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            location = LocationVO.objects.get(id=content["location"])
            print(location)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status = 400,
            )
        
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "POST"])
def api_detail_hats(request):
    if request.method == "DELETE":
        count, _= Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted: count>0"})
    elif request.method == "GET":
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status =400
            )
        Hat.objects.filter(id=pk).update(**content)
        hats =Hats.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)