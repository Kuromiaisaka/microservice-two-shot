import React from 'react'

class HatsList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            hats:[],
        };
        this.deleteHat = this.deleteHat.bind(this);
    }

    async deleteHat(hat) {
        const deleteUrl = `http://localhost:8090/api/hats/${hat.id}`;
        const fetchConfig = {
          method: "delete",
          }
        await fetch(deleteUrl, fetchConfig)
        const index = this.state.hats.indexOf(hat)
        const update_hats = [...this.state.hats]
        update_hats.splice(index,1)
        this.setState({ hats: update_hats })
        }
        

    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/hats/');
    
        if (response.ok) {
          const data = await response.json();
          this.setState({ hats: data.hats });
        }
      }


    render(){
        return (
            <>
            <h1>Hat Choices</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Fabric</th>
                        <th>Style</th>
                        <th>Color</th>
                        <th>Closet Name</th>
                        <th>Section Number</th>
                        <th>Shelf Number</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.hats.map(hats =>{
                        return (
                            <tr key = {hats.id}>
                                <td>{hats.name}</td>
                                <td>{hats.fabric}</td>
                                <td>{hats.style}</td>
                                <td>{hats.color}</td>
                                <td>{hats.location.close_name}</td>
                                <td>{hats.location.section_number}</td>
                                <td>{hats.location.shelf_number}</td>
                                <td><img src={hats.picture_url} /> </td>
                                <td><button className="btn-danger" onClick={()=> this.deleteHat(hats)}>Delete Hat</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
        );
    }
}

export default HatsList;