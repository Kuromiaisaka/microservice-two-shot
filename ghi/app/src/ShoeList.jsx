
import React from 'react';

class ShoesList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            manufacturer: '',
            model_number: '',
            color: '',
            picture_url: '',
            bins: [],
        };
        this.handleDelete = this.handleDelete.bind(this);
    }
    
    async componentDidMount() {
        const URL = 'http://localhost:8080/api/shoes_rest/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ shoes: data.shoes });
            console.log('componentDidMount:')
            console.log(data)
        }
    }
    async handleDelete(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log('HandleDelete of this data:')
        console.log(data)
        const shoeId = data.shoe;
        const deleteUrl = `http://localhost:8080/api/shoes_rest/${shoeId}`;
        const response = await fetch(deleteUrl, {method: "delete"});
        if (response.ok) {
           return(
            <div>
                <p>Your shoes have been deleted</p>
                <li className="nav-item">
                     <p>yay</p>
                </li>
            </div>
           )

          };
        }
      
    
    render() {
        return (
        <table className="table table-dark">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Bin</th>
                </tr>
            </thead>
        <tbody>
           {this.props.shoes.map(shoe => {
               return (
                    <tr key={shoe.href}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.bin.closet_name }</td>
                        <button onClick={this.handleDelete} className='btn btn-danger'>Delete</button>
                    </tr>
            );
        })} 
        </tbody>
      </table>
    );
  }
  
}
  export default ShoesList;