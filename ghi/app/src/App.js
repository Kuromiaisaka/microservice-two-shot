import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoeList';
import ShoeForm from './ShoeForm';
import Nav from './Nav';
import HatsForm from "./HatsForm";
import React from 'react';
import HatsList from "./HatsList";

function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <div className="container">
        <Routes>
          <Route path="/hats/list" element={<HatsList />} />
          <Route path="/hats/new" element={<HatsForm />} />
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoesList shoes={props.shoes} />} />
            <Route path="new" element={<ShoesList />} />
          </Route>
          <Route path="shoes">
            <Route path="detail" element={<ShoeForm />} />
        </Route>
        </Routes>
        </div>
    </BrowserRouter>
  );
}

export default App;
