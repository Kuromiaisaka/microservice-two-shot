import React from 'react';

class HatsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      fabric: '',
      style: '',
      color: '',
      picture_url: '',
      location: '',
      locations: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleFabricChange = this.handleFabricChange.bind(this);
    this.handleStyleChange = this.handleStyleChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.locations;

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHats = await response.json();
      console.log(newHats);
      this.setState({
        name: '',
        fabric: '',
        style: '',
        color: '',
        picture_url: '',
        location: '',
      });
    }
  }
  handleNameChange(event){
    const value = event.target.value;
    this.setState({name:value})
  }
  handleFabricChange(event) {
    const value = event.target.value;
    this.setState({ fabric: value });
  }

  handleStyleChange(event) {
    const value = event.target.value;
    this.setState({ style: value });
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handlePictureUrlChange(event) {
    const value = event.target.value;
    this.setState({ picture_url: value });
  }

  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStyleChange}  placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange}  placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureUrlChange} placeholder="PictureUrl" required type="text" name="image" id="image" className="form-control" />
                <label htmlFor="PictureUrl">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations ? this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.closet_name}</option>
                    )
                  }):null}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatsForm;
