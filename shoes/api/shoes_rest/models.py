from django.db import models
from django.urls import reverse


# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def get_api_url(self):
        return reverse("api_bin", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")



class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_number = models.PositiveIntegerField()
    color = models.CharField(max_length=30)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return f'{self.manufacturer} MODEL NUMBER:{self.model_number}'

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})