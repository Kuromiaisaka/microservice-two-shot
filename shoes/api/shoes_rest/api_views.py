from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer"]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


# Create your views here.
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_number",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":  
            shoes = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoeDetailEncoder,
            )
    else:
        content = json.loads(request.body)
        print(content)
        # Get the Bin object and put it in the content dict
        try:
            bin_href = content['bin']
            print(bin_href)
            bin = BinVO.objects.get(id=bin_href)
            content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id - does not exist triggered"},
                status=400,
            )
    
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})